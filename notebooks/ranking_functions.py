# -*- coding: utf-8 -*-
"""
Created on Tue Mar 24 11:08:18 2020

@author: heck_ti
"""
import sys, getopt
import numpy as np
import time
import pickle
def normalise_matrix(X):
    
    return np.nan_to_num((X.T / np.sum(X, axis=1)).T)
    
def rank_repositories_multiplicative(X, Xneg, M, O):
    
    #return O.multiply(Xneg.multiply(X.dot(np.exp(-M))).dot(X.T)).todense()
    return (X.dot(np.exp(-M)) * Xneg).dot(X.T) * O
    
def rank_repositories_rwr(X, Xneg, M, O, d=0.5):
    Xt = normalise_matrix(X.T) #csr_matrix(X.T / np.sum(X.T, axis=1))
    
    #return ((1 - d) * X.dot(Xt) + d * (Xneg.multiply(X.dot(np.exp(-M)))).dot(Xt)).todense()
    return (1 - d) * X.dot(Xt) + d * (X.dot(np.exp(-M)) * Xneg).dot(Xt)

def rank_repositories(repo_feature_matrix, ranking_function, **kwargs):
    start = time.time()
    print("start")
    print("normalisation")
    X = normalise_matrix(repo_feature_matrix)
    M = normalise_matrix(repo_feature_matrix.T.dot(repo_feature_matrix))
    Xneg = 1 - X #csr_matrix(1 - X)
    O = normalise_matrix(repo_feature_matrix.dot(repo_feature_matrix.T))
    #X = csr_matrix(X)
    #O = csr_matrix(normalise_matrix(repo_feature_matrix.dot(repo_feature_matrix.T)))
    print("ranking")
    couplings = ranking_function(X, Xneg, M, O, **kwargs)
    ranking = (-couplings).argsort(axis=None)
    print("done in {} seconds.".format(time.time() - start))
    return np.unravel_index(ranking, (couplings.shape[0], couplings.shape[1])) + (couplings.flatten()[ranking],)
#    return [(int(i / couplings.shape[1]), i % couplings.shape[1], 
#             couplings.flat[i]) 
#            for i in (-couplings).argsort(axis=None)]
    
#inputfile = ''
#outputfile = ''
#d = 0.5
#ranking_function = "multiplicative"
#
#try:
#   opts, args = getopt.getopt(sys.argv[1:],"hi:o:r:d:",["ifile=","ofile=","rfun"])
#except getopt.GetoptError:
#   print('ranking_functions.py -i <repo-feature matrix (csv)> -o <outputfile> -d <jumping probability (rwr only)> -r <ranking function [rwr|multiplicative (default)]>')
#   sys.exit(2)
#for opt, arg in opts:
#    if opt == '-h':
#        print('ranking_functions.py -i <repo-feature matrix (csv)> -o <outputfile> -d <jumping probability (rwr only)> -r <ranking function [rwr|multiplicative (default)]>')
#        sys.exit()
#    elif opt in ("-i", "--ifile"):
#        inputfile = arg
#    elif opt in ("-o", "--ofile"):
#        outputfile = arg
#    elif opt in ("-r", "--rfun"):
#        ranking_function = arg
#    elif opt == "-d":
#        d = float(arg)
#
#with open(inputfile) as f:
#    ncols = len(f.readline().split(','))
#    
#rf_matrix = np.loadtxt(inputfile, delimiter=",", skiprows=1, usecols=range(1,ncols))
#
#if (ranking_function == "rwr"):
#    res = rank_repositories(rf_matrix, rank_repositories_rwr, d=d)
#else:
#    res = rank_repositories(rf_matrix, rank_repositories_multiplicative)
#
#pickle.dump(res, outputfile + "_" + ranking_function + ".pickle")
##with open(outputfile + "_" + ranking_function + ".csv", "w") as fp:
##    fp.write("r1,r2,score\n")
##    fp.write("\n".join("{},{},{}".format(x[0],x[1],x[2]) for x in res))