# Repositories Synergies

This is the repository for the paper on finding synergies between repositories.


## Dataset

The data for this project can be downloaded from: https://dlrmax.dlr.de/files/2021/09/22/337992/b8e50cf2-0ea9-5598-9d1b-4ae7c310f513/data.zip 

We use the following data: 

* (1) **data/processed/repositories_with-readme_what-why-code.csv** contains all repos we use and 
* (2) **data/processed/repositories_with-readme_what-why-code_content.csv** contains al repos info as (1) in addition to each section contents/tags in the readme file and 
* (3) original readmefiles (md version with tags) in **data/readme_files/**
* (4) data/extracted_features/... contains extracted topics from the readme files
* (5) data/evaluation/... contains files that will be used for the evaluation of the appraoch 


The csv file (1) contains **14,065 repos** with the following characteristics:
    

*  Python as main language
*  Not deleted
*  At least 50 watchers
*  Updated in 2019
*  Availability of Readme files and has sections that describe features and repo purpose based on on the [READMEClassifer](https://github.com/gprana/READMEClassifier), *What, Why* section codes.


The csv file (2) contains **28,932** WhatWhy readme sections for  **14,065 repos** from (1).

### Steps taken to generate the dataset

#### 1. Fetching GitHub Dump


1.  We downloaded a dump from ghtorrent. We use only *Python* repositories that are not *deleted* and that they were *updated* in *2019*.
2.  We then loaded the repositories tables into a postgres database using ghtorrent sql script.

ghtorrent data is saved under *data/ghtorrent_python_notdeleted_repos_updated2019*.

#### 3. Fetching READMEFiles

We fetched the readme files using [PyGithub](https://github.com/PyGithub/PyGithub) for repositories that has at least *50 watchers* and we dismiss duplicate repos (11 duplicates).

We ended up with 20 509, saved in *data/processed/repositories_with-readme.csv*.

The readme files are saved under *data/readme_files*


#### 4. Classifying READMEFiles using [READMEClassifer](https://github.com/gprana/READMEClassifier)

We use the project [READMEClassifer](https://github.com/gprana/READMEClassifier) to classify the sections in the readme files under *data/readme_files*.

The classifications are saved under *data/ghtorrent_readme_classifications*.

#### 5. Extraction the contents of the readme files

The READMEClassifier extracts the contents of the readme files as a preprocessing step. They save the resulst in LightSql database. 

We exported the data into csv an saved them under *data/ghtorrent\_readme\_classifications/generated_data/* folder.

We then created 1 csv file with all the needed info using *5_extracting_readme_content.ipynb* and saved them here *data/processed/repositories_with-readme_what-why-code_content.csv*.



## Notebooks

jupyter notebooks are under *notebooks/*:

*  *1_fetching-ghtorrent.ipynb*: a python script to save ghtorrent dump into postgres db.

*  *2_fetching-readme.ipynb*: a python script to fetch readme files.

*  *3_extending-repos-info.ipynb*: a python script combine the info of repos into one place.

*  *4_exploring-ghtorrent-readme-classification.ipynb*: a python script to explore the readme file classification

*  *5_extracting_readme_content.ipynb*: a python script to create 1 csv file with repo info and sections contents

*  *6_1_NMF_topic_extraction.ipynb*: a python script to extract topics from readme files using non-negative matrix factorisation
* Annotation evaluation. Evaluation script.

## Pre-requisites

For reproducing the data creation:
     
1. Install pygithub
 `pip install PyGithub`
2. Get Github Personal Access Token
    * Github restricts default requests to be 60 req/hr. with PAT, 5000 requests/hr is possible. 
    * Go to https://github.com/settings/tokens and generate a new token
    * set the token with environment variable 'PAT' eg. set PAT=xxxxxxxxxxxxxxxxxxxxxx


